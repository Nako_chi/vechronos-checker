import abi from './abi.json' assert { type: 'json' };

const provider = new ethers.JsonRpcProvider('https://arb1.arbitrum.io/rpc', undefined, { batchMaxCount: 1 });
const contract = new ethers.Contract('0x9a01857f33aa382b1d5bb96c3180347862432b0d', abi, provider);

document.querySelector('form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const submitButton = document.getElementById('submit-button');
  submitButton.disabled = true;
  submitButton.value = 'Checking...';

  const tokenIds = e.target.elements.tokenIds.value.trim().split(/\r\n|\n/);
  const results = await Promise.all(tokenIds.map(async (tokenId) => {
    const locked = await contract.locked(tokenId);
    return { tokenId, amount: locked.amount };
  }));

  let html = '';
  for (const result of results) {
    html += `<div>${result.tokenId}: ${ethers.formatUnits(result.amount, 18)}</div>`;
  }

  submitButton.disabled = false;
  submitButton.value = 'Check';
  e.target.elements.tokenIds.value = '';

  const output = document.getElementById('output');
  output.innerHTML = html + output.innerHTML;
});
